import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"

const IndexPage = () => (
  <Layout>
    <h1>Você sabia que o pai do Chris Rock tem DOIS empregos?!</h1>
    <p>Pois, é... O pai do Chris tem DOIS empregos!</p>
    <StaticImage
      src="../images/julius.jpg"
      width={600}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />
    <div>version: %%VERSION%%</div>
  </Layout>
)

export default IndexPage
